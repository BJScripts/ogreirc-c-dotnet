function main()
{
    Event[OgreIRC_MessageReceived]:AttachAtom[OgreIRC_MessageReceived]

    while 1
        waitframe
}

atom(global) OgreIRC_MessageReceived(string _speaker, string _channelName, string _message)
{
    echo ${Time}: _speaker: [${_speaker}]
    echo ${Time}: _channelName: [${_channelName}]
    echo ${Time}: _message: [${_message}]
}