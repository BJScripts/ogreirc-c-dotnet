﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using InnerSpaceAPI;
using LavishScriptAPI;
using Meebey.SmartIrc4net;
using OgreIRC.Classes;

namespace OgreIRC
{
    
    class Program
    {
        // the server we want to connect to, could be also a simple string
        public static string[] serverlist = { "irc.ogregaming.com" };
        private static int port = 6667;
        private static string channel = "#bjtest";
        private static string username = "BJIRCBOT";
        private static string realname = "BJ";

        static void Main(string[] args)
        {
            //Add Lavishscript Commands
            LavishScript.Commands.AddCommand("IRC", SendToIRC);
            
            //Add Lavishscript Events
            LavishScript.Events.RegisterEvent("OgreIRC_MessageReceived");
            
            // UTF-8 test
            Classes.OgreIRC.irc.Encoding = System.Text.Encoding.UTF8;

            // wait time between messages, we can set this lower on own irc servers
            Classes.OgreIRC.irc.SendDelay = 200;

            // we use channel sync, means we can use irc.GetChannel() and so on
            Classes.OgreIRC.irc.ActiveChannelSyncing = true;

            // here we connect the events of the API to our written methods
            // most have own event handler types, because they ship different data
            Classes.OgreIRC.irc.OnQueryMessage += new IrcEventHandler(Classes.OgreIRC.OnQueryMessage);
            Classes.OgreIRC.irc.OnError += new ErrorEventHandler(Classes.OgreIRC.OnError);
            Classes.OgreIRC.irc.OnRawMessage += new IrcEventHandler(Classes.OgreIRC.OnRawMessage);

            try
            {
                // here we try to connect to the server and exceptions get handled
                Classes.OgreIRC.irc.Connect(serverlist, port);
            }
            catch (ConnectionException e)
            {
                // something went wrong, the reason will be shown
                System.Console.WriteLine("couldn't connect! Reason: " + e.Message);
                Classes.OgreIRC.Exit();
            }

            try
            {
                // here we logon and register our nickname and so on 
                Classes.OgreIRC.irc.Login(username, realname);
                // join the channel
                Classes.OgreIRC.irc.RfcJoin(channel);

                /*
                for (int i = 0; i < 3; i++)
                {
                    // here we send just 3 different types of messages, 3 times for
                    // testing the delay and flood protection (messagebuffer work)
                    Classes.OgreIRC.irc.SendMessage(SendType.Message, channel, "test message (" + i.ToString() + ")");
                    Classes.OgreIRC.irc.SendMessage(SendType.Action, channel, "thinks this is cool (" + i.ToString() + ")");
                    Classes.OgreIRC.irc.SendMessage(SendType.Notice, channel, "SmartIrc4net rocks (" + i.ToString() + ")");
                }
                */
                // spawn a new thread to read the stdin of the console, this we use
                // for reading IRC commands from the keyboard while the IRC connection
                // stays in its own thread
                new Thread(new ThreadStart(Classes.OgreIRC.ReadCommands)).Start();

                // here we tell the IRC API to go into a receive mode, all events
                // will be triggered by _this_ thread (main thread in this case)
                // Listen() blocks by default, you can also use ListenOnce() if you
                // need that does one IRC operation and then returns, so you need then 
                // an own loop 
                Classes.OgreIRC.irc.Listen();

                // when Listen() returns our IRC session is over, to be sure we call
                // disconnect manually
                Classes.OgreIRC.irc.Disconnect();
            }
            catch (ConnectionException)
            {
                // this exception is handled because Disconnect() can throw a not
                // connected exception
                Classes.OgreIRC.Exit();
            }
            catch (Exception e)
            {
                // this should not happen by just in case we handle it nicely
                System.Console.WriteLine("Error occurred! Message: " + e.Message);
                System.Console.WriteLine("Exception: " + e.StackTrace);
                Classes.OgreIRC.Exit();
            }
        }

        private static int SendToIRC(string[] args)
        {
            // arg[0] = CommandName (ie. IRC)
            // arg[1] = Message to Send
            Classes.OgreIRC.irc.SendMessage(SendType.Message, channel, args[1]);
            return 1;
        }
    }
}
